#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

#pragma pack(push, 1)
struct pixel {
    int8_t b, g, r;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image init(const uint64_t h, const uint64_t w);

struct image copy(const struct image * const source);

void destructor_image(struct image *image);

#endif
