#include "bmpio.h"
#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
    if (argc != 4 || (strcmp(argv[3], "90") !=0 && strcmp(argv[3], "-90") !=0 && strcmp(argv[3], "180") !=0 &&
        strcmp(argv[3], "-180") !=0 && strcmp(argv[3], "270") !=0 && strcmp(argv[3], "-270") !=0 && strcmp(argv[3], "0") !=0)) {
        fprintf(stderr,
                "Incorrect arguments. Enter the arguments in the following format: ./image-transformer <source-image> <transformed-image> <angle>");
        return 0;
    }
    struct image img;
    switch (read_bmp(argv[1], &img)) {
        case READ_OK:
            break;
        case READ_INVALID_SIGNATURE:
            fprintf(stderr, "Error when reading the file body");
            destructor_image(&img);
            return READ_INVALID_SIGNATURE;
        case READ_INVALID_HEADER:
            fprintf(stderr, "Error when reading the file header");
            destructor_image(&img);
            return READ_INVALID_HEADER;
        case READ_INVALID_FILE:
            fprintf(stderr, "Error opening the sours file");
            destructor_image(&img);
            return READ_INVALID_FILE;
        default:
            fprintf(stderr, "Error reading the file");
            destructor_image(&img);
            return READ_INVALID_FILE;
    }
    struct image rotate_img;
    int64_t angle = (strtol(argv[3], NULL, 10) + 360) % 360;
    switch (angle) {
        case 0:
            rotate_img = copy(&img);
            break;
        case 90:
            rotate_img = rotate_90(&img);
            break;
        case 180:
            rotate_img = rotate_180(&img);
            break;
        case 270:
            rotate_img = rotate_270(&img);
            break;
    }
    switch (write_bmp(argv[2],&rotate_img)) {
        case WRITE_OK:
            destructor_image(&img);
            destructor_image(&rotate_img);
            break;
        case WRITE_ERROR:
            fprintf(stderr, "Error opening the converted file");
            destructor_image(&img);
            destructor_image(&rotate_img);
            return WRITE_ERROR;
    }
    return 0;
}
