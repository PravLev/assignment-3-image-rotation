#include "bmp.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


const struct bmp_header bmp_header_st =
        {.bfType = 19778,
                .bfReserved = 0,
                .bOffBits = sizeof(struct bmp_header),
                .biSize = 40,
                .biPlanes = 1,
                .biBitCount = 24,
                .biCompression = 0,
                .biXPelsPerMeter = 2834,
                .biYPelsPerMeter = 2834,
                .biClrUsed = 0,
                .biClrImportant = 0};

uint64_t padding(uint32_t wight) {
    return (4 - (wight * 3) % 4) % 4;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) < 1) {
        return READ_INVALID_HEADER;
    }

    char *input_img = malloc(header.biSizeImage);
    if (fread(input_img, header.biSizeImage, 1, in) < 1) {
        free(input_img);
        return READ_INVALID_SIGNATURE;
    }
    size_t p = padding(header.biWidth);
    *img = init(header.biHeight, header.biWidth);

    for (uint64_t i = 0; i < header.biHeight; ++i) {
        for (uint64_t j = 0; j < header.biWidth; ++j) {
            img->data[header.biWidth * i + j] = (struct pixel)
                    {input_img[(header.biWidth * 3 + p) * i + j * 3],
                     input_img[(header.biWidth * 3 + p) * i + j * 3 + 1],
                     input_img[(header.biWidth * 3 + p) * i + j * 3 + 2]};
        }
    }
    free(input_img);
    return READ_OK;

}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header header = bmp_header_st;
    header.biSizeImage = (img->width * sizeof(struct pixel) + padding(img->width)) * img->height;
    header.bfileSize = header.biSizeImage + header.bOffBits;
    header.biWidth = img->width;
    header.biHeight = img->height;

    if (fwrite(&header, sizeof(header), 1, out) < 1) {
        return WRITE_ERROR;
    }
    char *output_img = malloc(header.biSizeImage);
    uint64_t p = padding(img->width);
    for (uint64_t i = 0; i < img->height; ++i) {
        for (uint64_t j = 0; j < img->width; ++j) {
            output_img[(img->width * 3 + p) * i + j * 3] = img->data[img->width * i + j].b;
            output_img[(img->width * 3 + p) * i + j * 3 + 1] = img->data[img->width * i + j].g;
            output_img[(img->width * 3 + p) * i + j * 3 + 2] = img->data[img->width * i + j].r;
        }
        for (uint64_t j = 0; j < p; ++j) {
            output_img[(img->width * 3 + p) * i + img->width * 3 + j] = 0;
        }
    }

    if (fwrite(output_img, 1, header.biSizeImage, out) < header.biSizeImage) {
        free(output_img);
        return WRITE_ERROR;
    }
    free(output_img);
    return WRITE_OK;

}
