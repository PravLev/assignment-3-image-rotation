#include "rotate.h"
#include <stdint.h>

struct image rotate_270(struct image const *const source) {
    struct image r270 = init(source->width, source->height);
    for (uint64_t i = 0; i < source->height; ++i) {
        for (uint64_t j = 0; j < source->width; ++j) {
            r270.data[r270.width * (j + 1) - 1 - i] = source->data[i * source->width + j];
        }
    }
    return r270;
}

struct image rotate_180(struct image const *const source) {
    struct image r180 = init(source->height, source->width);
    for (uint64_t i = 0; i < source->height; ++i) {
        for (uint64_t j = 0; j < source->width; ++j) {
            r180.data[r180.width * r180.height - 1 - (i * r180.width + j)] = source->data[i * source->width + j];
        }
    }
    return r180;
}

struct image rotate_90(struct image const *const source) {
    struct image r90 = init(source->width, source->height);
    for (uint64_t i = 0; i < r90.height; ++i) {
        for (uint64_t j = 0; j < r90.width; ++j) {
            r90.data[i * r90.width + j] = source->data[source->width * (j + 1) - 1 - i];
        }
    }
    return r90;
}
