#include "image.h"
#include <stdint.h>
#include <stdlib.h>


struct image init(const uint64_t h, const uint64_t w) {
    return (struct image) {
            .width = w,
            .height = h,
            .data = malloc(sizeof(struct pixel) * w * h)
    };
}

struct image copy(const struct image * const source) {
    struct image img = init(source->height, source->width);
    for (uint64_t i = 0; i < img.width * img.height; ++i) {
        img.data[i] = source->data[i];
    }
    return img;
}

void destructor_image(struct image *image) {
    if (image->data == NULL)
        return;
    free(image->data);
    image->height = 0;
    image->width = 0;
    image->data = NULL;
}
