#include "bmpio.h"
enum read_status read_bmp(char *filename, struct image *img) {
    FILE *input = fopen(filename, "rb");
    if (!input)
        return READ_INVALID_FILE;
    enum read_status rs = from_bmp(input, img);
    fclose(input);
    return rs;
}

enum write_status write_bmp(char *filename, struct image *img) {
    FILE *output = fopen(filename, "wb");
    if(!output)
        return WRITE_ERROR;
    enum write_status ws = to_bmp(output, img);
    fclose(output);
    return ws;
}

